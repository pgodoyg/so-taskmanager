﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskManager
{
    public partial class TaskManager : Form
    {
        List <string> listaProcBloqueados;
        
        public TaskManager()
        {
            InitializeComponent();
            listaProcBloqueados = new List<string>();
        
            listarProcesos();
        }

        /// <summary>
        /// Metodo: Se encarga de matar un proceso y mostrar un mensaje de alerta.
        /// </summary>
        /// <param name="id">Id del proceso que se desea matar</param>
        /// <param name="motivo">El motivo de cancelación del proceso</param>
        /// <returns>String: nombre del proceso cancelado</returns>
        private string matarProceso(int id, String motivo)
        {
            String proceso = " ";
            try
            {
                proceso = Process.GetProcessById(id).ProcessName;
                Process.GetProcessById(id).Kill();
                MessageBox.Show("¡Se cancelo la ejeucion del programa '" + proceso + "'!", "Task Manager : " + motivo);
            }
            catch (Exception e)
            {
                MessageBox.Show("No se cerro el proces, permiso denegado!" + System.Environment.NewLine + e.ToString(), "Permiso Denegado");
            }
            return proceso;
        }

        /// <summary>
        /// Metodo: Obtiene los procesos utilizando las bibliotecas propias del sistema y los carga al GridProcesos posterior a limpiarlo.
        ///         Adicional, verifica si el proceso esta o no en la lista de bloqueados
        /// </summary>
        private void listarProcesos(){
            int cantProcesos = Process.GetProcesses().Count();

            if (cantProcesos != Int32.Parse(StripLabelContProcesos.Text))
            {
                GridProcesosEjecucion.Rows.Clear();
                StripLabelContProcesos.Text = cantProcesos.ToString();

                foreach (Process p in Process.GetProcesses())
                {
                    if (listaProcBloqueados.Contains(p.ProcessName))
                    {
                        matarProceso(p.Id, "Proceso Bloqueado");
                    }

                    GridProcesosEjecucion.Rows.Add(p.Id, p.ProcessName);
                }

                try
                {
                    GridProcesosEjecucion.Sort(GridProcesosEjecucion.Columns["ProcActivoProceso"], ListSortDirection.Ascending);
                }
                catch (Exception e)
                {
                    MessageBox.Show("¡No se pudo ordenar los procesos por nombre!" + System.Environment.NewLine + e.ToString(), "Error en el ordenamiento");
                }
                
            }
        }

        /// <summary>
        /// Metodo: Recarga el grid de procesos bloqueados
        /// </summary>
        private void recargarProcBloqueados()
        {
            GridProcesosBloqueados.Rows.Clear();
            foreach (String nombre in listaProcBloqueados)
            {
                GridProcesosBloqueados.Rows.Add(nombre);
            }
        }

        private void ButtonActualizar_Click(object sender, EventArgs e)
        {
            listarProcesos();
        }

        private void ButtonMatar_Click(object sender, EventArgs e)
        {
            matarProceso((int)GridProcesosEjecucion.Rows[GridProcesosEjecucion.SelectedCells[0].RowIndex].Cells[0].Value, "Proceso Cancelado");
        }

        private void ButtonBloquear_Click(object sender, EventArgs e)
        {
            listaProcBloqueados.Add(matarProceso((int)GridProcesosEjecucion.Rows[GridProcesosEjecucion.SelectedCells[0].RowIndex].Cells[0].Value, "Proceso Bloqueado"));

            recargarProcBloqueados();
        }

        private void ButtonDesBloquear_Click(object sender, EventArgs e)
        {
            if (listaProcBloqueados.Count > 0)
            {
                listaProcBloqueados.Remove(GridProcesosBloqueados.Rows[GridProcesosBloqueados.SelectedCells[0].RowIndex].Cells[0].Value.ToString());

                recargarProcBloqueados();
            }
        }

        private void monitor_Tick(object sender, EventArgs e)
        {
            listarProcesos();
        }

        
    }
}
