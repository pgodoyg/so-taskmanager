﻿namespace TaskManager
{
    partial class TaskManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskManager));
            this.Tabs = new System.Windows.Forms.TabControl();
            this.TabProcesosEjecucion = new System.Windows.Forms.TabPage();
            this.GridProcesosEjecucion = new System.Windows.Forms.DataGridView();
            this.ProcActivoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcActivoProceso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonActualizar = new System.Windows.Forms.Button();
            this.TabProcesosBloqueados = new System.Windows.Forms.TabPage();
            this.ButtonDesBloquear = new System.Windows.Forms.Button();
            this.GridProcesosBloqueados = new System.Windows.Forms.DataGridView();
            this.ProcBloqueado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Strip = new System.Windows.Forms.StatusStrip();
            this.StripLabelTotalProcesos = new System.Windows.Forms.ToolStripStatusLabel();
            this.StripLabelContProcesos = new System.Windows.Forms.ToolStripStatusLabel();
            this.monitor = new System.Windows.Forms.Timer(this.components);
            this.ButtonBloquear = new System.Windows.Forms.Button();
            this.ButtonMatar = new System.Windows.Forms.Button();
            this.Tabs.SuspendLayout();
            this.TabProcesosEjecucion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridProcesosEjecucion)).BeginInit();
            this.TabProcesosBloqueados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridProcesosBloqueados)).BeginInit();
            this.Strip.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.TabProcesosEjecucion);
            this.Tabs.Controls.Add(this.TabProcesosBloqueados);
            this.Tabs.Location = new System.Drawing.Point(12, 12);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(403, 434);
            this.Tabs.TabIndex = 0;
            // 
            // TabProcesosEjecucion
            // 
            this.TabProcesosEjecucion.Controls.Add(this.ButtonBloquear);
            this.TabProcesosEjecucion.Controls.Add(this.ButtonMatar);
            this.TabProcesosEjecucion.Controls.Add(this.GridProcesosEjecucion);
            this.TabProcesosEjecucion.Controls.Add(this.ButtonActualizar);
            this.TabProcesosEjecucion.Location = new System.Drawing.Point(4, 22);
            this.TabProcesosEjecucion.Name = "TabProcesosEjecucion";
            this.TabProcesosEjecucion.Padding = new System.Windows.Forms.Padding(3);
            this.TabProcesosEjecucion.Size = new System.Drawing.Size(395, 408);
            this.TabProcesosEjecucion.TabIndex = 0;
            this.TabProcesosEjecucion.Text = "En Ejecucion";
            this.TabProcesosEjecucion.UseVisualStyleBackColor = true;
            // 
            // GridProcesosEjecucion
            // 
            this.GridProcesosEjecucion.AllowUserToAddRows = false;
            this.GridProcesosEjecucion.AllowUserToDeleteRows = false;
            this.GridProcesosEjecucion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridProcesosEjecucion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProcActivoId,
            this.ProcActivoProceso});
            this.GridProcesosEjecucion.Location = new System.Drawing.Point(3, 3);
            this.GridProcesosEjecucion.Name = "GridProcesosEjecucion";
            this.GridProcesosEjecucion.ReadOnly = true;
            this.GridProcesosEjecucion.Size = new System.Drawing.Size(389, 356);
            this.GridProcesosEjecucion.TabIndex = 0;
            // 
            // ProcActivoId
            // 
            this.ProcActivoId.HeaderText = "Id";
            this.ProcActivoId.Name = "ProcActivoId";
            this.ProcActivoId.ReadOnly = true;
            // 
            // ProcActivoProceso
            // 
            this.ProcActivoProceso.HeaderText = "Proceso";
            this.ProcActivoProceso.Name = "ProcActivoProceso";
            this.ProcActivoProceso.ReadOnly = true;
            // 
            // ButtonActualizar
            // 
            this.ButtonActualizar.Image = global::TaskManager.Properties.Resources.actualizar;
            this.ButtonActualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonActualizar.Location = new System.Drawing.Point(3, 362);
            this.ButtonActualizar.Name = "ButtonActualizar";
            this.ButtonActualizar.Size = new System.Drawing.Size(80, 40);
            this.ButtonActualizar.TabIndex = 4;
            this.ButtonActualizar.Text = "Actualizar";
            this.ButtonActualizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonActualizar.UseVisualStyleBackColor = true;
            this.ButtonActualizar.Click += new System.EventHandler(this.ButtonActualizar_Click);
            // 
            // TabProcesosBloqueados
            // 
            this.TabProcesosBloqueados.Controls.Add(this.ButtonDesBloquear);
            this.TabProcesosBloqueados.Controls.Add(this.GridProcesosBloqueados);
            this.TabProcesosBloqueados.Location = new System.Drawing.Point(4, 22);
            this.TabProcesosBloqueados.Name = "TabProcesosBloqueados";
            this.TabProcesosBloqueados.Padding = new System.Windows.Forms.Padding(3);
            this.TabProcesosBloqueados.Size = new System.Drawing.Size(395, 408);
            this.TabProcesosBloqueados.TabIndex = 1;
            this.TabProcesosBloqueados.Text = "Bloqueados";
            this.TabProcesosBloqueados.UseVisualStyleBackColor = true;
            // 
            // ButtonDesBloquear
            // 
            this.ButtonDesBloquear.Location = new System.Drawing.Point(160, 375);
            this.ButtonDesBloquear.Name = "ButtonDesBloquear";
            this.ButtonDesBloquear.Size = new System.Drawing.Size(75, 23);
            this.ButtonDesBloquear.TabIndex = 4;
            this.ButtonDesBloquear.Text = "Desbloquear";
            this.ButtonDesBloquear.UseVisualStyleBackColor = true;
            this.ButtonDesBloquear.Click += new System.EventHandler(this.ButtonDesBloquear_Click);
            // 
            // GridProcesosBloqueados
            // 
            this.GridProcesosBloqueados.AllowUserToAddRows = false;
            this.GridProcesosBloqueados.AllowUserToDeleteRows = false;
            this.GridProcesosBloqueados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridProcesosBloqueados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProcBloqueado});
            this.GridProcesosBloqueados.Location = new System.Drawing.Point(3, 3);
            this.GridProcesosBloqueados.Name = "GridProcesosBloqueados";
            this.GridProcesosBloqueados.ReadOnly = true;
            this.GridProcesosBloqueados.Size = new System.Drawing.Size(389, 366);
            this.GridProcesosBloqueados.TabIndex = 0;
            // 
            // ProcBloqueado
            // 
            this.ProcBloqueado.HeaderText = "Proceso";
            this.ProcBloqueado.Name = "ProcBloqueado";
            this.ProcBloqueado.ReadOnly = true;
            // 
            // Strip
            // 
            this.Strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StripLabelTotalProcesos,
            this.StripLabelContProcesos});
            this.Strip.Location = new System.Drawing.Point(0, 449);
            this.Strip.Name = "Strip";
            this.Strip.Size = new System.Drawing.Size(446, 22);
            this.Strip.TabIndex = 1;
            this.Strip.Text = "statusStrip1";
            // 
            // StripLabelTotalProcesos
            // 
            this.StripLabelTotalProcesos.Name = "StripLabelTotalProcesos";
            this.StripLabelTotalProcesos.Size = new System.Drawing.Size(100, 17);
            this.StripLabelTotalProcesos.Text = "Total de Procesos";
            // 
            // StripLabelContProcesos
            // 
            this.StripLabelContProcesos.Name = "StripLabelContProcesos";
            this.StripLabelContProcesos.Size = new System.Drawing.Size(13, 17);
            this.StripLabelContProcesos.Text = "0";
            // 
            // monitor
            // 
            this.monitor.Enabled = true;
            this.monitor.Interval = 1000;
            this.monitor.Tick += new System.EventHandler(this.monitor_Tick);
            // 
            // ButtonBloquear
            // 
            this.ButtonBloquear.Image = global::TaskManager.Properties.Resources.bloquear;
            this.ButtonBloquear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonBloquear.Location = new System.Drawing.Point(311, 362);
            this.ButtonBloquear.Name = "ButtonBloquear";
            this.ButtonBloquear.Size = new System.Drawing.Size(80, 40);
            this.ButtonBloquear.TabIndex = 3;
            this.ButtonBloquear.Text = "Bloquear";
            this.ButtonBloquear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonBloquear.UseVisualStyleBackColor = true;
            this.ButtonBloquear.Click += new System.EventHandler(this.ButtonBloquear_Click);
            // 
            // ButtonMatar
            // 
            this.ButtonMatar.Image = global::TaskManager.Properties.Resources.cancel;
            this.ButtonMatar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonMatar.Location = new System.Drawing.Point(157, 362);
            this.ButtonMatar.Name = "ButtonMatar";
            this.ButtonMatar.Size = new System.Drawing.Size(80, 40);
            this.ButtonMatar.TabIndex = 5;
            this.ButtonMatar.Text = "Matar";
            this.ButtonMatar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonMatar.UseVisualStyleBackColor = true;
            this.ButtonMatar.Click += new System.EventHandler(this.ButtonMatar_Click);
            // 
            // TaskManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 471);
            this.Controls.Add(this.Strip);
            this.Controls.Add(this.Tabs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TaskManager";
            this.Text = "Task Manager";
            this.Tabs.ResumeLayout(false);
            this.TabProcesosEjecucion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridProcesosEjecucion)).EndInit();
            this.TabProcesosBloqueados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridProcesosBloqueados)).EndInit();
            this.Strip.ResumeLayout(false);
            this.Strip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage TabProcesosEjecucion;
        private System.Windows.Forms.DataGridView GridProcesosEjecucion;
        private System.Windows.Forms.TabPage TabProcesosBloqueados;
        private System.Windows.Forms.StatusStrip Strip;
        private System.Windows.Forms.ToolStripStatusLabel StripLabelTotalProcesos;
        private System.Windows.Forms.ToolStripStatusLabel StripLabelContProcesos;
        private System.Windows.Forms.Button ButtonBloquear;
        private System.Windows.Forms.Button ButtonActualizar;
        private System.Windows.Forms.Button ButtonMatar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcActivoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcActivoProceso;
        private System.Windows.Forms.DataGridView GridProcesosBloqueados;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcBloqueado;
        private System.Windows.Forms.Button ButtonDesBloquear;
        private System.Windows.Forms.Timer monitor;

    }
}

